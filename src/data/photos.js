export const photos = [{
        src: "img/villamarilena/outside1.jpg",
        width: 5,
        height: 3
    },
    {
        src: "img/villamarilena/outside2.jpg",
        width: 3,
        height: 2
    },
    {
        src: "img/villamarilena/outside3.jpg",
        width: 4,
        height: 4
    },
    {
        src: "img/villamarilena/outside4.jpg",
        width: 3,
        height: 4
    },
    {
        src: "img/villamarilena/bedroom1.jpg",
        width: 6,
        height: 4
    },
    {
        src: "img/villamarilena/bedroom2.jpg",
        width: 2.6,
        height: 3
    },
    {
        src: "img/villamarilena/bedroom3.jpg",
        width: 2,
        height: 3
    },
    {
        src: "img/villamarilena/bedroom4.jpg",
        width: 3,
        height: 3
    },
    {
        src: "img/villamarilena/bedroom5.jpg",
        width: 3,
        height: 3
    },
    {
        src: "img/villamarilena/kitchen1-large.jpg",
        width: 4,
        height: 3
    }, 
    {
        src: "img/villamarilena/microwave.jpg",
        width: 3,
        height: 3
    },
    {
        src: "img/villamarilena/Diningtable.jpg",
        width: 3,
        height: 4
    },
    {
        src: "img/villamarilena/livingroom.jpg",
        width: 3,
        height: 4
    },
    {
        src: "img/villamarilena/entrance.jpg",
        width: 3,
        height: 4
    },
    {
        src: "img/villamarilena/villaAtNightHor-large.jpg",
        width: 4,
        height: 3
    },
    {
        src: "img/villamarilena/villaNight2.jpg",
        width: 3,
        height: 3
    },
    {
        src: "img/villamarilena/sight1.jpg",
        width: 3,
        height: 3
    },
    {
        src: "img/villamarilena/sight8.jpg",
        width: 4,
        height: 4
    },
    {
        src: "img/villamarilena/sight6.jpg",
        width: 4,
        height: 4
    },
    {
        src: "img/villamarilena/strand.jpg",
        width: 3,
        height: 3
    },
    {
        src: "img/Korfu_sights.jpg",
        width: 3,
        height: 3
    }
    
];